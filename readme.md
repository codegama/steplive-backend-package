## About Project

A Self Hosted Whitelabel Video Conferencing Suite - Cost and Security

Step.Live is an innovative approach to the video conferencing paradigm. While you get all the bells and whistles that Zoom, Microsoft Teams or Skype has to offer, you get none of their bills. Also, you can sleep peacefully knowing all your internal discussions, client communications etc are all in your own company servers.

## Setup Project

- composer update

- nano .env (update database and app url's)

- php artisan config:cache

- php artisan clear-compiled

- composer dump-autoload

- php artisan migrate 

- php artisan db:seed

- php artisan storage:link

## More Details

Refer trello board - https://trello.com/b/SBaJMBsc