<?php

use Illuminate\Database\Seeder;

class BBBRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
    		[
		        'key' => 'BBB_RECORD_PRESENTATION_URL_PRO',
		        'value' => 'https://media.step.live/download/presentation/'
		    ],
		    [
		        'key' => 'BBB_RECORD_PRESENTATION_URL_DEBUG',
		        'value' => 'https://media.appswamy.com/download/presentation/'
		    ],
		    [
		        'key' => 'BBB_RECORD_VIDEO_URL_PRO',
		        'value' => 'https://media.appswamy.com/presentation/'
		    ],
		    [
		        'key' => 'BBB_RECORD_VIDEO_URL_DEBUG',
		        'value' => 'https://media.appswamy.com/presentation/'
		    ],
		]);
    }
}
