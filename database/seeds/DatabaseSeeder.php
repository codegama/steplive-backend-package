<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingSeeder::class);
        $this->call(DemoSeeder::class);
        $this->call(StaticPageDemoSeeder::class);
        $this->call(PageDemoSeeder::class);
        $this->call(BBBRecordSeeder::class);
    }
}
