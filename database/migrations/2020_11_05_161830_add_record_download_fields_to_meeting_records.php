<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecordDownloadFieldsToMeetingRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meeting_records', function (Blueprint $table) {
            $table->string('overall_record_url')->default("")->after('playback_url');
            $table->string('video_record_url')->default("")->after('overall_record_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meeting_records', function (Blueprint $table) {
            $table->dropColumn('overall_record_url');
            $table->dropColumn('video_record_url');
        });
    }
}
