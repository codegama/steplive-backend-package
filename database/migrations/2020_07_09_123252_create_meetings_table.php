<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unique_id')->default(rand());
            $table->integer('user_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('picture')->default(asset('meeting-placeholder.jpg'));
            $table->string('meeting_type')->default(MEETING_TYPE_LIVE);
            $table->string('video')->default('');
            $table->dateTime('schedule_time')->nullable();
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->integer('no_of_users')->default(1);
            $table->float('no_of_minutes')->default(1)->comment("in minutes");
            $table->string('created_by')->default('user');
            $table->tinyInteger('is_cancelled')->default(0);
            $table->text('cancelled_reason')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}
