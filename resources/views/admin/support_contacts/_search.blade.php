<form  action="{{route('admin.support_contacts.index')}}" method="GET" role="search">

<div class="row pt-2 pb-2">

    <div class="col-6">
        @if(Request::has('search_key'))
            <p class="text-muted">Search results for <b>{{Request::get('search_key')}}</b></p>
        @endif
    </div>
    

    <div class="col-6">

        <div class="input-group">
            <input type="text" class="form-control search-input" name="search_key"
                placeholder="{{tr('contacts_search_placeholder')}}"> <span class="input-group-btn">
                &nbsp

                <button type="submit" class="btn btn-primary btn-width">
                   {{tr('search')}}
                </button>

                <a class="btn btn-primary" href="{{route('admin.support_contacts.index')}}">{{tr('clear')}}
                </a>
            </span>
        </div>

    </div>

</div>

</form>