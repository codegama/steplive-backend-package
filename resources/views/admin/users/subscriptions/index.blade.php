@extends('layouts.admin')

@section('content-header', tr('users'))

@section('bread-crumb')

    <li class="breadcrumb-item">
    	<a href="{{ route('admin.users.index') }}">{{tr('users')}}</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
    	<span>{{tr('subscription_history')}}</span>
    </li>
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">


                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">{{tr('subscription_history')}}

                        - <a href="{{route('admin.users.view' , ['user_id' => $user_details->id ?? 0])}}">{{$user_details->name}}</a>
                  
                    </h5>

                </div>

                <div class="pt-3 pb-3">
                    <table id="custom-datatable" class="table dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th>{{tr('s_no')}}</th>
                                <th>{{tr('payment_id')}}</th>
                                <th>{{tr('subscription')}}</th>
                                <th>{{tr('plan')}}</th>
                                <th>{{tr('amount')}}</th> 
                                <th>{{tr('expiry_date')}}</th>        
                                <th>{{tr('action')}}</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach($subscripton_payment_history as $i => $payment_details)
                                  
                                <tr>
                                    <td>{{$i+$subscripton_payment_history->firstItem()}}</td>

                                    <td>
                                        <a href="{{route('admin.subscription_payments.view' , ['subscription_payment_id' => $payment_details->id ?? 0])}}">{{$payment_details->payment_id}}</a>
                                    </td>

                                    <td>
                                        <a href="{{route('admin.subscriptions.view' , ['subscription_id' => $payment_details->subscriptionDetails->id ?? 0])}}"> {{ $payment_details->subscriptionDetails->title ?? '-' }}
                                        </a>
                                    </td>

                                    <td> {{$payment_details->plan_formatted}} </td>

                                    <td>{{$payment_details->subscription_amount_formatted}}</td>

                                    <td class="text-danger">{{common_date($payment_details->expiry_date,Auth::guard('admin')->user()->timezone)}}</td>

                                    <td>     
                                        @if(Setting::get('is_demo_control_enabled') == NO)
                                      
                                            <a class="btn btn-danger" href="{{route('admin.users.subscriptions_payments_delete', ['subscription_payment_id' => $payment_details->id])}}" 
                                            onclick="return confirm(&quot;{{tr('subscription_history_delete_confirmation')}}&quot;);">
                                                {{tr('delete')}}
                                            </a>
                                        @endif

                                    </td>

                                </tr>

                            @endforeach

                        </tbody>
                       
                    </table>

                    <div class="float-right">{{ $subscripton_payment_history->links() }}</div>

                </div>
                
            </div>
            
        </div>
        
    </div>
   
</div>


<div class="row">

	@foreach($subscriptions as $subscription_details)

		<div class="col-md-2 sub_box_css" >   

			<div class="card">

			   <div class="card-body">

			      	<h3 class="text-center text-uppercase">{{$subscription_details->title}}</h3>

                    <div class="media mt-1 border-top pt-3">

                        <div class="media-body">

                            <h6 class="mt-1 mb-0 font-size-15">{{tr('no_of_users')}}</h6>
                            <h6 class="text-muted font-weight-normal mt-1 mb-3">{{$subscription_details->no_of_users_formatted}}</h6>

                            <h6 class="mt-1 mb-0 font-size-15">{{tr('no_of_minutes')}}</h6>
                            <h6 class="text-muted font-weight-normal mt-1 mb-3">{{$subscription_details->no_of_minutes_formatted}}</h6>

                            <h6 class="mt-1 mb-0 font-size-15">{{tr('plan')}}</h6>
                            <h6 class="text-muted font-weight-normal mt-1 mb-3">{{$subscription_details->plan_formatted}}</h6>

                            <h6 class="mt-1 mb-0 font-size-15">{{tr('amount')}}</h6>

                            <h6 class="text-muted font-weight-normal mt-1 mb-3">{{$subscription_details->amount_formatted}}</h6>

                        </div>

                    </div>

					<form action="{{route('admin.users.subscriptions_payments')}}" method="POST">
						@csrf
						<input type="hidden" name="subscription_id" value="{{$subscription_details->id}}">

						<input type="hidden" name="user_id" value="{{Request::get('user_id')}}">

						<button class="btn btn-danger btn-block" type="submit" onclick="return confirm('{{tr('do_you_want_to_subscribe_this_plan')}}')">{{tr('subscribe')}}</button>

					</form>

			   </div>

			</div>
	           
	    </div>

    @endforeach

</div>

@endsection