@extends('layouts.admin') 

@section('content-header', tr('payments'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{route('admin.revenue.dashboard')}}">{{tr('payments')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('subscription_payments') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-body">

                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">{{tr('subscription_payments')}}
                    </h5>

                </div>

                @include('admin.payments.subscription_payments._search')

                <table id="custom-datatable" class="table dt-responsive nowrap">

                    <thead>

                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('subscription')}}</th>
                            <th>{{tr('payment_id')}}</th>
                            <th>{{tr('username')}}</th>
                            <th>{{tr('amount')}}</th>
                            <th>{{tr('payment_mode')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>

                    </thead>

                    <tbody>

                        @foreach($subscription_payments as $i => $subscription_payment_details)

                            <tr>
                                <td>{{$i+$subscription_payments->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.subscriptions.view' , ['subscription_id' => $subscription_payment_details->subscription_id])}}"> {{ $subscription_payment_details->subscriptionDetails->title ?? "-"}}
                                    </a>
                                </td>

                                <td> 
                                   <a href="{{ route('admin.subscription_payments.view', ['subscription_payment_id' => $subscription_payment_details->subscription_payment_id]) }}">
                                  {{ $subscription_payment_details->payment_id }} 
                                   </a>
                                  </td>

                                <td>  
                                    <a href="{{route('admin.users.view' , ['user_id' => $subscription_payment_details->user_id])}}"> {{ $subscription_payment_details->userDetails->name ?? "-" }} 
                                    </a>
                                </td>

                                <td> 
                                    {{ formatted_amount($subscription_payment_details->amount) }} 
                                </td>

                                <td> 
                                    <span  class="badge badge-info">{{ $subscription_payment_details->payment_mode  }} </span>
                                </td>

                                <td>

                                    @if($subscription_payment_details->status == USER_APPROVED)

                                        <span class="badge badge-success">{{ tr('paid') }} </span>

                                    @else

                                        <span class="badge badge-danger">{{ tr('unpaid') }} </span>

                                    @endif

                                </td>

                                <td>     

                                    <a class="btn btn-info" href="{{ route('admin.subscription_payments.view', ['subscription_payment_id' => $subscription_payment_details->subscription_payment_id]) }}">
                                    {{tr('view')}}</a>

                                </td>

                            </tr>

                        @endforeach
                        
                    </tbody>
                   
                </table>

                <div class="float-right">{{ $subscription_payments->appends(Request::all())->links() }}</div>

            </div>
            
        </div>
        
    </div>
    
</div>

@endsection