@extends('layouts.admin')

@section('content-header', tr('meetings'))

@section('bread-crumb')

<li class="breadcrumb-item"><a href="{{ route('admin.meetings.index')}}">{{tr('meetings')}}</a></li>

@if(Request::get('type'))

<li class="breadcrumb-item active" aria-current="page">
    <span>{{ tr('scheduled_meetings') }}</span>
</li>

@else
<li class="breadcrumb-item active" aria-current="page">
    <span>{{ tr('meetings_history') }}</span>
</li>
@endif

@endsection

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">{{Request::get('type') ? tr('scheduled_meetings') : tr('meetings_history')}}

                        <div class="dropdown float-right">
                            <button class="btn btn-outline-primary  dropdown-toggle float-right bulk-action-dropdown text-uppercase" href="#" id="dropdownMenuOutlineButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus"></i> {{tr('bulk_action')}}
                            </button>

                            <div class="dropdown-menu float-right" aria-labelledby="dropdownMenuOutlineButton2">

                                <a class="dropdown-item action_list" href="#" id="bulk_delete">
                                    {{tr('delete')}}
                                </a>

                                <a class="dropdown-item action_list" href="#" id="bulk_meeting_end">
                                    {{ tr('meeting_end') }}
                                </a>

                            </div>
                        </div>

                        <div class="bulk_action">

                            <form action="{{route('admin.meetings.bulk_action')}}" id="meetings_form" method="POST" role="search">

                                @csrf

                                <input type="hidden" name="action_name" id="action" value="">

                                <input type="hidden" name="selected_meetings" id="selected_ids" value="">

                                <input type="hidden" name="page_id" id="page_id" value="{{ (request()->page) ? request()->page : '1' }}">

                            </form>

                        </div>



                    </h5>

                </div>

                @include('admin.meetings._search')

                <table id="custom-datatable" class="table dt-responsive nowrap">

                    <thead>

                        <tr>
                            <th>
                                @if($meetings->count() >= 1)
                                <input id="check_all" type="checkbox" class="chk-box-left">
                                @endif
                            </th>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('unique_id')}}</th>
                            <th>{{tr('title')}}</th>
                            <th>{{tr('user')}}</th>
                            <th>{{tr('schedule_time')}}</th>
                            <th>{{tr('total_users')}}</th>
                            @if(!Request::get('type'))
                            <th>{{tr('status')}}</th>
                            @endif
                            <th>{{tr('record')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>

                    </thead>

                    <tbody>

                        @foreach($meetings as $i => $meeting_details)

                        <tr>
                            <td id="check{{$meeting_details->id}}"><input type="checkbox" name="row_check" class="faChkRnd chk-box-inner-left" id="{{$meeting_details->id}}" value="{{$meeting_details->id}}"></td>

                            <td>{{$i+$meetings->firstItem()}}</td>

                            <td>
                                <a href="{{route('admin.meetings.view',['meeting_id' => $meeting_details->id])}}"> {{ $meeting_details->unique_id }}
                                </a>
                            </td>

                            <td>

                                <a href="{{route('admin.meetings.view',['meeting_id' => $meeting_details->id])}}">
                                    {{ $meeting_details->title }}
                                </a>
                            </td>

                            <td>
                                <a href="{{route('admin.users.view',['user_id' => $meeting_details->user_id])}}"> {{ $meeting_details->userDetails->name ?? "-" }}
                                </a>
                            </td>

                            <td>
                                {{common_date($meeting_details->schedule_time,Auth::guard('admin')->user()->timezone)}}

                            </td>

                            <td>
                                {{ $meeting_details->users_count }}
                            </td>

                            @if(!Request::get('type'))
                            <td>

                                <span class="badge badge-secondary"> {{$meeting_details->status_formatted}}</span>

                            </td>
                            @endif

                            <td>

                                <span class="badge badge-info"> {{$meeting_details->is_recordings_formatted}}</span>

                            </td>

                            <td>

                                <div class="template-demo">

                                    <div class="dropdown">
>>>>>>> remotes/codegama/steplive-backend-base/master

                                        <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{tr('action')}}
                                        </button>

                                        <div class="dropdown-menu" aria-labelledby="actionDropdown">

                                            <a class="dropdown-item" href="{{ route('admin.meetings.view', ['meeting_id' => $meeting_details->id]) }}">
                                                {{tr('view')}}
                                            </a>

                                            @if($meeting_details->is_recordings == YES)

                                            <a class="dropdown-item" href="{{ route('admin.meeting_records.view', ['meeting_id' => $meeting_details->id]) }}">
                                                {{tr('view_record')}}
                                            </a>

                                            @endif

                                            @if($meeting_details->status != MEETING_ENDED)
                                            <a class="dropdown-item" href="{{ route('admin.meetings.edit', ['meeting_id' => $meeting_details->id]) }}">
                                                {{tr('edit')}}
                                            </a>

                                            <a class="dropdown-item" href="{{route('admin.meetings.delete', ['meeting_id' => $meeting_details->id])}}" onclick="return confirm(&quot;{{tr('meeting_delete_confirmation' , $meeting_details->title)}}&quot;);">
                                                {{tr('delete')}}
                                            </a>

                                            @endif

                                        </div>

                                    </div>

                                </div>

                            </td>

                        </tr>

                        @endforeach

                    </tbody>

                </table>

                <div class="float-right">{{ $meetings->appends(request()->input())->links() }}</div>

            </div>

        </div>

    </div>

</div>


@endsection


@section('scripts')

@if(Session::has('bulk_action'))
<script type="text/javascript">
    $(document).ready(function() {
        localStorage.clear();
    });
</script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        get_values();

        // Call to Action for Delete || Schedule || Start || End
        $('.action_list').click(function() {
            var selected_action = $(this).attr('id');
            if (selected_action != undefined) {
                $('#action').val(selected_action);
                if ($("#selected_ids").val() != "") {
                    if (selected_action == 'bulk_delete') {
                        var message = "{{ tr('admin_meetings_delete_confirmation') }}";
                    } else if (selected_action == 'bulk_meeting_end') {
                        var message = "{{ tr('admin_meetings_end_confirmation') }}";
                    }
                    var confirm_action = confirm(message);

                    if (confirm_action == true) {
                        $("#meetings_form").submit();
                    }
                    // 
                } else {
                    alert('Please select the check box');
                }
            }
        });
        // single check
        var page = $('#page_id').val();
        $('.faChkRnd:checkbox[name=row_check]').on('change', function() {

            var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
                    return this.id;
                })
                .get();

            localStorage.setItem("meetings_checked_items" + page, JSON.stringify(checked_ids));

            get_values();

        });
        // select all checkbox
        $("#check_all").on("click", function() {
            if ($("input:checkbox").prop("checked")) {
                $("input:checkbox[name='row_check']").prop("checked", true);
                var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
                        return this.id;
                    })
                    .get();
                console.log("meetings_checked_items" + page);

                localStorage.setItem("meetings_checked_items" + page, JSON.stringify(checked_ids));
                get_values();
            } else {
                $("input:checkbox[name='row_check']").prop("checked", false);
                localStorage.removeItem("meetings_checked_items" + page);
                get_values();
            }

        });

        // Get Id values for selected meetings
        function get_values() {
            var pageKeys = Object.keys(localStorage).filter(key => key.indexOf('meetings_checked_items') === 0);
            var values = Array.prototype.concat.apply([], pageKeys.map(key => JSON.parse(localStorage[key])));

            if (values) {
                $('#selected_ids').val(values);
            }

            for (var i = 0; i < values.length; i++) {
                $('#' + values[i]).prop("checked", true);
            }
        }

        $('.faChkRnd').on("click", function() {

            var value = $(this).val();

            $('#check' + value).trigger('click');
        });

    });
</script>

@endsection