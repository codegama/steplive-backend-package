 <div class="left-side-menu">

    <div class="media user-profile mt-2 mb-2">
        <img src="{{Auth::guard('admin')->user()->picture}}" class="avatar-sm rounded-circle mr-2" alt="{{Auth::guard('admin')->user()->name}}" />
        <img src="{{Auth::guard('admin')->user()->picture}}" class="avatar-xs rounded-circle mr-2" alt="{{Auth::guard('admin')->user()->name}}" />

        <div class="media-body">
            <h6 class="pro-user-name mt-0 mb-0">{{Auth::guard('admin')->user()->name}}</h6>
            <span class="pro-user-desc">Administrator</span>
        </div>
        
    </div>

    <div class="sidebar-content">

        <div id="sidebar-menu" class="slimscroll-menu">

            <ul class="metismenu" id="menu-bar">

                <li>
                    <a href="{{route('admin.dashboard')}}">
                        <i data-feather="home"></i>
                        <span>{{tr('dashboard')}}</span>
                    </a>
                </li>
               
                <li id="users-crud">

                    <a href="javascript: void(0);">
                        <i data-feather="user"></i>
                        <span>{{tr('users')}}</span>
                        <span class="menu-arrow"></span>
                    </a>

                    <ul class="nav-second-level" aria-expanded="false" id="users">
                        <li>
                            <a href="{{route('admin.users.create')}}" id="users-create">
                                {{tr('add_user')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.users.index')}}" id="users-view">
                                {{tr('view_users')}}
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-title">{{tr('meeting_management')}}</li>

                <li id="meetings-crud">

                    <a href="javascript: void(0);">
                        <i data-feather="phone"></i>
                        <span>{{tr('meetings')}}</span>
                        <span class="menu-arrow"></span>
                    </a>
                    
                    <ul class="nav-second-level" aria-expanded="false" id="meetings">

                        <li>
                            <a href="{{route('admin.meetings.index')}}" id="meetings-index">
                                {{tr('meetings_history')}}
                            </a>
                        </li>

                        <li>
                            <a href="{{route('admin.meeting_records.index')}}" id="meetings-records">
                                {{tr('meeting_records')}}
                            </a>
                        </li>

                        <li>
                            <a href="{{route('admin.meetings.index', ['type' => 'scheduled'])}}" id="meetings-scheduled">
                                {{tr('scheduled_meetings')}}
                            </a>
                        </li>

                    </ul>

                </li>
                
                <li class="menu-title">{{tr('revenue_management')}}</li>

                <li id="subscriptions-crud">
                    <a href="javascript: void(0);">
                        <i data-feather="key" class="icon-dual"></i>
                        <span>{{tr('subscriptions')}}</span>
                        <span class="menu-arrow"></span>
                    </a>

                    <ul class="nav-second-level" aria-expanded="false" id="subscriptions">
                        <li>
                            <a href="{{route('admin.subscriptions.create')}}" id="subscriptions-create">
                                {{tr('add_subscription')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.subscriptions.index')}}" id="subscriptions-view">
                                {{tr('view_subscriptions')}}
                            </a>
                        </li>
                    </ul>
                </li>

                <li id="payments-crud">
                    <a href="javascript: void(0);">
                        <i data-feather="credit-card" class="icon-dual"></i>
                        <span>{{tr('payments')}}</span>
                        <span class="menu-arrow"></span>
                    </a>

                    <ul class="nav-second-level" aria-expanded="false" id="payments">

                        <li>
                            <a href="{{route('admin.revenue.dashboard')}}"  id="revenues-dashboard">
                                {{tr('revenue_dashboard')}}
                            </a>
                        </li>

                        <li>
                            <a href="{{route('admin.subscription_payments.index')}}" id="subscription-payments">
                                {{tr('subscription_payments')}}
                            </a>
                        </li>

                    </ul>

                </li>

                <li class="menu-title">{{tr('setting_management')}}</li>

                <li id="static_pages-crud">
                    <a href="javascript: void(0);">
                        <i data-feather="file-text" class="icon-dual"></i>
                        <span>{{tr('static_pages')}}</span>
                        <span class="menu-arrow"></span>
                    </a>

                    <ul class="nav-second-level" aria-expanded="false" id="static_pages">

                        <li>
                            <a href="{{route('admin.static_pages.create')}}"  id="static_pages-create">
                                {{tr('add_static_page')}}
                            </a>
                        </li>

                        <li>
                            <a href="{{route('admin.static_pages.index')}}" id="static_pages-view">
                                {{tr('view_static_pages')}}
                            </a>
                        </li>

                    </ul>

                </li>

                <li id="support_contacts-crud">
                    <a class="" href="{{route('admin.support_contacts.index')}}" aria-expanded="false">
                        <i data-feather="search"></i>
                        <span>{{tr('support_contacts')}} </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.settings')}}" aria-expanded="false">
                        <i data-feather="settings"></i>
                        <span>{{tr('settings')}} </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.profile')}}" aria-expanded="false">
                        <i data-feather="star"></i>
                        <span> {{tr('profile')}} </span>
                    </a>
                </li>

                <li>
                    <a data-toggle="modal" data-target="#logoutModel" aria-expanded="false">
                        <i data-feather="log-out"></i>
                        <span> {{tr('logout')}} </span>
                    </a>
                </li>
               
            </ul>

        </div>

        <div class="clearfix"></div>

    </div>

</div>
