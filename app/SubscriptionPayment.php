<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPayment extends Model
{
    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['subscription_payment_id', 'subscription_payment_unique_id', 'plan_formatted', 'subscription_amount_formatted', 'no_of_users_formatted', 'no_of_minutes_formatted', 'currency', 'status_formatted', 'subscription_name'];

    public function getSubscriptionPaymentIdAttribute() {

        return $this->id;
    }

    public function getSubscriptionPaymentUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getPlanFormattedAttribute() {

        return formatted_plan($this->plan, $this->plan_type);
    }

    public function getSubscriptionAmountFormattedAttribute() {

        return formatted_amount($this->amount);
    }

    public function getNoOfUsersFormattedAttribute() {

        return no_of_users_formatted($this->no_of_users);
    }

    public function getNoOfMinutesFormattedAttribute() {

        return no_of_minutes_formatted($this->no_of_minutes);
    }

    public function getCurrencyAttribute() {

        return \Setting::get('currency' , '$');
    }

    public function getStatusFormattedAttribute() {

        return $this->status == PAID ? tr('paid') : tr('unpaid');
    }

    public function getSubscriptionNameAttribute() {

        $name = $this->subscriptionDetails->title ?? tr('unavailable');

        unset($this->subscriptionDetails);

        return $name;
    }

    /**
     * Get the subscription details 
     */
	public function subscriptionDetails() {

		return $this->belongsTo(Subscription::class,'subscription_id');

	}

    /**
     * Get the subscription details 
     */
    public function userDetails() {

        return $this->belongsTo(User::class,'user_id');

    }

	/**
     * Scope a query to basic subscription details
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBaseResponse($query) {

    	return $query;
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "SP-"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "SP-"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    }
}
