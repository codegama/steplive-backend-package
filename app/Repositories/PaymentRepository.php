<?php

namespace App\Repositories;

use App\Helpers\Helper;

use Log, Validator, Setting, Exception, DB;

class PaymentRepository {

    /**
     * @method subscriptions_payment_save()
     *
     * @uses used to save user subscription payment details
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $subscription_details, object $request
     *
     * @return object $subscription_details
     */

    public static function subscriptions_payment_save($request, $subscription_details) {

        try {

            \App\SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->update(['is_current_subscription' => NO]);

            $previous_payment = \App\SubscriptionPayment::where('user_id', $request->id)
                                            ->where('status', PAID)
                                            ->orderBy('created_at', 'desc')
                                            ->first();

            $subscription_payment = new \App\SubscriptionPayment;

            $plan_type = $subscription_details->plan_type ?? PLAN_TYPE_MONTH; // For future purpose, dont remove

            $subscription_payment->expiry_date = date('Y-m-d H:i:s',strtotime("+{$subscription_details->plan} {$plan_type}"));

            if($previous_payment) {

                if (strtotime($previous_payment->expiry_date) >= strtotime(date('Y-m-d H:i:s'))) {
                    $subscription_payment->expiry_date = date('Y-m-d H:i:s', strtotime("+{$subscription_details->plan} {$plan_type}", strtotime($previous_payment->expiry_date)));
                }
            }

            $subscription_payment->subscription_id = $request->subscription_id;

            $subscription_payment->user_id = $request->id;

            $subscription_payment->payment_id = $request->payment_id ?? "NO-".rand();

            $subscription_payment->status = PAID;

            $subscription_payment->amount = $request->paid_amount ?? 0.00;

            $subscription_payment->payment_mode = $request->payment_mode ?? CARD;

            $subscription_payment->is_current_subscription = YES;

            $subscription_payment->no_of_users = $subscription_details->no_of_users;

            $subscription_payment->no_of_minutes = $subscription_details->no_of_minutes;

            $subscription_payment->plan = $subscription_details->plan;

            $subscription_payment->plan_type = $subscription_details->plan_type;

            $subscription_payment->cancel_reason = "";
            
            $subscription_payment->paid_date = date('Y-m-d H:i:s');

            $subscription_payment->save();

            $response = ['success' => true, 'message' => 'paid', 'data' => ['subscription_type' => PAID_USER, 'payment_id' => $subscription_payment->payment_id]];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }
    
    }

    /**
     * @method subscriptions_payment_by_stripe()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $subscription_details, object $request
     *
     * @return object $subscription_details
     */

    public static function subscriptions_payment_by_stripe($request, $subscription_details) {

        try {

            // Check stripe configuration
        
            $stripe_secret_key = Setting::get('stripe_secret_key');

            if(!$stripe_secret_key) {

                throw new Exception(api_error(107), 107);

            } 

            \Stripe\Stripe::setApiKey($stripe_secret_key);
           
            $currency_code = Setting::get('currency_code', 'USD') ?: "USD";

            $total = intval(round($request->user_pay_amount * 100));

            $charge_array = [
                'amount' => $total,
                'currency' => $currency_code,
                'customer' => $request->customer_id,
                "payment_method" => $request->card_token,
                'off_session' => true,
                'confirm' => true,
            ];

            $stripe_payment_response = \Stripe\PaymentIntent::create($charge_array);

            $payment_data = [
                                'payment_id' => $stripe_payment_response->id ?? 'CARD-'.rand(),
                                'paid_amount' => $stripe_payment_response->amount/100 ?? $total,

                                'paid_status' => $stripe_payment_response->paid ?? true
                            ];

            $response = ['success' => true, 'message' => 'done', 'data' => $payment_data];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

}