<?php

namespace App\Http\Controllers\UserApi;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Repositories\MeetingRepository as MeetingRepo;

use DB, Log, Hash, Validator, Exception, Setting, Helper;

use App\User;

class MeetingApiController extends Controller
{
    protected $loginUser;

    protected $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

        \Log::info("timezone".$this->timezone);

    }

    /**
     * @method meetings_index()
     *
     * @uses list rooms created by logged user
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_index(Request $request) {

        try {

            $meeting_ids = \App\MeetingUser::where('user_id', $request->id)->pluck('meeting_id');

            $base_query = $total_query = \App\Meeting::where('meetings.user_id', $request->id)->orderBy('meetings.created_at', 'desc');

            if($meeting_ids) {
                $base_query = $base_query->orWhereIn('meetings.id', $meeting_ids);
            }

            $meetings = $base_query->skip($this->skip)->take($this->take)->get();

            $request->request->add(['timezone' => $this->timezone]);

            $meetings = MeetingRepo::meetings_list_response($meetings, $request);

            $data['meetings'] = $meetings;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_upcoming()
     *
     * @uses list rooms created by logged user
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_upcoming(Request $request) {

        try {

            $base_query = $total_query = \App\Meeting::where('user_id', $request->id)->where('status', MEETING_SCHEDULED)->orderBy('created_at', 'desc');

            $meetings = $base_query->skip($this->skip)->take($this->take)->get();

            $request->request->add(['timezone' => $this->timezone]);

            $meetings = MeetingRepo::meetings_list_response($meetings, $request);

            $data['meetings'] = $meetings;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_view()
     *
     * @uses room view based on the logged in user
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_view(Request $request) {

        try {

            $meeting_details = \App\Meeting::where('meetings.unique_id', $request->meeting_unique_id)->first();

            $meeting_details->start_btn_status = $meeting_details->end_btn_status = $meeting_details->cancel_btn_status =$meeting_details->join_btn_status = $meeting_details->is_owner = NO;

            if($request->id == $meeting_details->user_id) {

                $meeting_details->is_owner = YES;

                $meeting_details->start_btn_status = $meeting_details->status == MEETING_SCHEDULED ? YES : NO;

                $meeting_details->end_btn_status = $meeting_details->status == MEETING_STARTED ? YES : NO;

                $meeting_details->cancel_btn_status = in_array($meeting_details->status, [MEETING_SCHEDULED]) ? YES : NO;

            } else {

                $meeting_details->join_btn_status = $meeting_details->status == MEETING_STARTED ? YES : NO;

            }

            $meeting_details->schedule_time = $meeting_details->schedule_time_formatted = $meeting_details->schedule_time ? common_date($meeting_details->schedule_time, $this->timezone, 'd M Y h:i A') : "";

            $meeting_details->start_time = $meeting_details->start_time_formatted = $meeting_details->start_time ? common_date($meeting_details->start_time, $this->timezone, 'd M Y h:i A') : "";

            $meeting_details->end_time = $meeting_details->end_time_formatted = $meeting_details->end_time ? common_date($meeting_details->end_time, $this->timezone, 'd M Y h:i A') : "";

            $record_details = \App\MeetingRecord::where('meeting_id', $meeting_details->meeting_id)->first() ?? [];

            $meeting_details->overall_record_url = $record_details->overall_record_url ?? "";
            
            $meeting_details->video_record_url = $record_details->video_record_url ?? "";

            $data['meeting_details'] = $meeting_details;


            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }
    
    /**
     * @method meetings_now_save()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_now_save(Request $request) {

        try {

            // Check the user is subscribed any plan

            DB::beginTransaction();

            $meeting_details = new \App\Meeting;

            $meeting_details->user_id = $request->id;

            $meeting_details->title = $request->title ?: "My Meeting ".rand(10000, 99999);

            $meeting_details->description = $request->description ?: "My Meeting ".rand(10000, 99999);

            $meeting_details->schedule_time = $meeting_details->start_time = date('Y-m-d H:i:s');

            $subscription_payment = \App\SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->first();

            $no_of_users = Setting::get('max_no_of_users', 3);

            $no_of_minutes = Setting::get('max_no_of_minutes', 30);

            if($subscription_payment) {

                $no_of_users = $subscription_payment->no_of_users ?? 5;

                $no_of_minutes = $subscription_payment->no_of_minutes ?? 5;

            }

            $meeting_details->no_of_users = $no_of_users;

            $meeting_details->no_of_minutes = $no_of_minutes;

            $meeting_details->status = MEETING_STARTED;

            $meeting_details->save();

            if(Setting::get('is_jitsi', 0) == 0) {

                $response = \App\Repositories\BBBRepository::bbb_meeting_create($request, $meeting_details)->getData();

                if($response->success == false) {

                    throw new Exception($response->error, $response->error_code);
                }

            }
            
            $meeting_user = new \App\MeetingUser;

            $meeting_user->meeting_id = $meeting_details->meeting_id;

            $meeting_user->user_id = $request->id ?: 0;

            $meeting_user->username = $this->loginUser->name ?? "User".rand(10000, 99999);

            $meeting_user->start_time = date('Y-m-d H:i:s');

            $meeting_user->save();

            $attendeePW = $moderatorPW = '';

            if($request->id == $meeting_details->user_id) {

                $moderatorPW = 'mp'.$meeting_details->meeting_id;

            } else {

                $attendeePW = 'ap'.$meeting_details->meeting_id;

            }

            if(Setting::get('is_jitsi', 0) == 0) {

                $request->request->add(['username' => $meeting_user->username,'moderatorPW' => $moderatorPW, 'attendeePW' => $attendeePW]);

                $response = \App\Repositories\BBBRepository::bbb_meeting_join($request, $meeting_details, $meeting_user)->getData();

                if($response->success == false) {

                    throw new Exception($response->error, $response->error_code);
                }

                $data['bbb_response'] = $response;

                $data['join_url'] = $response->data;

            }

            DB::commit();

            $meeting_details->is_jitsi = Setting::get('is_jitsi', 0);

            $data['meeting_details'] = $meeting_details;

            DB::commit();

            return $this->sendResponse($message = api_success(119) , $code = 119, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_save()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_save(Request $request) {

        try {

            // Check the user is subscribed any plan

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'title' => 'required|max:255',
                    'description' => 'required|max:255',
                    // 'schedule_time' => 'nullable',
                    // 'picture' => 'nullable|mimes:jpeg,jpg,png',
                ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $schedule_time = $request->schedule_time ? common_server_date($request->schedule_time, $this->timezone) : date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")." +5 minutes"));

            // $current_time = common_date(date('Y-m-d H:i:s'), $this->timezone);

            if(strtotime($schedule_time) <= strtotime(date('Y-m-d H:i:s'))) {

                throw new Exception(api_error(146), 146);
                
            }

            // Validation end

            $meeting_details = \App\Meeting::find($request->meeting_id) ?? new \App\Meeting;

            $meeting_details->user_id = $request->id;

            $meeting_details->title = $request->title;

            $meeting_details->description = $request->description ?? "";

            $meeting_details->schedule_time = $schedule_time;

            $subscription_payment = \App\SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->first();

            $no_of_users = Setting::get('max_no_of_users', 3);

            $no_of_minutes = Setting::get('max_no_of_minutes', 30);

            if($subscription_payment) {

                $no_of_users = $subscription_payment->no_of_users ?? 5;

                $no_of_minutes = $subscription_payment->no_of_minutes ?? 5;

            }

            $meeting_details->no_of_users = $no_of_users;

            $meeting_details->no_of_minutes = $no_of_minutes;

            if($request->file('picture')) {

                $meeting_details->picture = Helper::storage_upload_file($request->file('picture'), MEETING_PATH);

            }

            $meeting_details->save();

            if(Setting::get('is_jitsi', 0) == 0) {

                $response = \App\Repositories\BBBRepository::bbb_meeting_create($request, $meeting_details)->getData();

                if($response->success == false) {

                    throw new Exception($response->error, $response->error_code);
                }


                $meeting_details->bbb_internal_meeting_id = $response->data->internalMeetingID ?? "";
                
                $meeting_details->bbb_voice_bridge = $response->data->voiceBridge ?? "";

                $meeting_details->bbb_dial_number = $response->data->dialNumber ?? "";

                $data['bbb_response'] = $response ?? [];
            }

            $meeting_details->save();

            DB::commit();

            $data['meeting_details'] = $meeting_details;

            $code = $request->meeting_id ? 129 : 119;

            return $this->sendResponse($message = api_success($code) , $code, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }
    
    /**
     * @method meetings_joing()
     *
     * @uses join the meeting room
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_join(Request $request) {

        try {

            // Check the user is subscribed any plan

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_unique_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting_details = \App\Meeting::where('meetings.unique_id', $request->meeting_unique_id)->first();

            if(!$meeting_details) {
                throw new Exception(api_error(139), 139);
            }

            $user_id = $request->id ?: 0;

            if(($meeting_details->status != MEETING_STARTED) && ($meeting_details->user_id != $user_id)) {

                $error = get_meeting_status_error($meeting_details->status);

                throw new Exception($error, 141);

            }

            $meeting_user = new \App\MeetingUser;

            $meeting_user->meeting_id = $meeting_details->meeting_id;

            $meeting_user->user_id = $request->id ?: 0;

            $meeting_user->username = $request->username ?: ($this->loginUser->name ?? "User".rand(10000, 99999));

            $meeting_user->start_time = date('Y-m-d H:i:s');

            $meeting_user->save();

            $attendeePW = $moderatorPW = '';

            if($request->id == $meeting_details->user_id) {

                $moderatorPW = 'mp'.$meeting_details->meeting_id;

                $meeting_details->start_time = date('Y-m-d H:i:s');

                $meeting_details->status = MEETING_STARTED;

                $meeting_details->save(); 

            } else {

                $attendeePW = 'ap'.$meeting_details->meeting_id;

            }

            $request->request->add(['username' => $meeting_user->username,'moderatorPW' => $moderatorPW, 'attendeePW' => $attendeePW]);

            $response = \App\Repositories\BBBRepository::bbb_meeting_join($request, $meeting_details, $meeting_user)->getData();

            if($response->success == false) {

                throw new Exception($response->error, $response->error_code);
            }

            DB::commit();

            $meeting_details->is_jitsi = Setting::get('is_jitsi', 0);

            $data['meeting_details'] = $meeting_details;

            $data['join_url'] = $response->data;

            return $this->sendResponse($message = api_success(125) , $code = 125, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_end()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_end(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting = \App\Meeting::where('unique_id', $request->meeting_id)->first();

            if(!$meeting) {
                throw new Exception(api_error(139), 139);
            }

            $meeting->status = MEETING_ENDED;

            $meeting->start_time = $meeting->start_time ?: date('Y-m-d H:i:s');

            $meeting->end_time = date('Y-m-d H:i:s');

            $meeting->save();

            DB::commit();

            $frontend_url = Setting::get('frontend_url').'meetings';

            return redirect()->away($frontend_url);

            return $this->sendResponse($message = api_success(119) , $code = 119, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            $frontend_url = Setting::get('frontend_url').'meetings';

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_users_logout()
     *
     * @uses the other users logout @todo this function not completed, because not able to identify which user is logging out from BBB
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_users_logout(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting_details = \App\Meeting::where('unique_id', $request->meeting_id)->first();

            if(!$meeting_details) {
                throw new Exception(api_error(139), 139);
            }

            $meeting_user = \App\MeetingUser::find($request->meeting_user_id);

            if($meeting_user) {

                $meeting_user->status = MEETING_USER_LEFT;

                $meeting_user->save();

                if($meeting_user->user_id == $meeting_details->user_id) {

                    $meeting_details->status = MEETING_ENDED;

                    $meeting_details->save();
                }
            }

            DB::commit();

            $frontend_url = Setting::get('frontend_url').'meetings';

            return redirect()->away($frontend_url);

        } catch(Exception $e) {

            DB::rollback();

            $frontend_url = Setting::get('frontend_url').'meetings';

            return redirect()->away($frontend_url);
        
        }
    
    }

    /**
     * @method meetings_delete()
     *
     * @uses sub instructors remove
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_delete(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_id' => 'required|exists:meetings,id'];

            $custom_errors = ['meeting_id' => api_error(139)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $meeting_details = \App\Meeting::where('meetings.id', $request->meeting_id)->where('user_id', $request->id)->delete();

            DB::commit();

            return $this->sendResponse($message = api_success(120) , $code = 120, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_search()
     *
     * @uses meetings search
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_search(Request $request) {

        try {

            // Validation start

            $rules = ['search_key' => 'required|min:2'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $base_query = $total_query = \App\Meeting::where('title','LIKE','%'.$request->search_key.'%');

            $meetings = $base_query->skip($this->skip)->take($this->take)->get();

            $data['meetings'] = $meetings;

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_users_index()
     *
     * @uses list users under selected meeting
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_users_index(Request $request) {

        try {

            // Validation start

            $rules = ['meeting_id' => 'required|exists:meetings,id'];

            $custom_errors = ['meeting_id' => api_error(132)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $total_query = $base_query = \App\MeetingUser::where('meeting_id', $request->meeting_id);

            $meeting_users = $base_query->skip($this->skip)->take($this->take)->get();

            $data['meeting_users'] = $meeting_users;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_users_search()
     *
     * @uses meeting users search based on the key
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_users_search(Request $request) {

        try {

            // Validation start

            $rules = [
                        'search_key' => 'required|min:2', 
                        'meeting_id' => 'required|exists:meetings,id'
                    ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting_user_ids = \App\MeetingUser::where('meeting_id', $request->meeting_id)->list('user_id');

            $base_query = $total_query = \App\User::whereNotIn('users.id', '!=', $meeting_user_ids)
                            ->where('name','LIKE','%'.$request->search_key.'%')
                            ->orWhere('email','LIKE','%'.$request->search_key.'%')
                            ->orWhere('mobile','LIKE','%'.$request->search_key.'%');

            $users = $user_query->skip($this->skip)->take($this->take)->get();

            $data['users'] = $users;

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_records_index()
     *
     * @uses list of the recordings
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_records_index(Request $request) {

        try {

            $meeting_ids = \App\MeetingUser::where('user_id', $request->id)->pluck('meeting_id');

            $base_query = $total_query = \App\MeetingRecord::where('user_id', $request->id)->orderBy('created_at', 'desc');

            if($meeting_ids) {
                
                $base_query = $base_query->orWhereIn('meeting_records.meeting_id', $meeting_ids);
            }

            $meeting_records = $base_query->skip($this->skip)->take($this->take)->get();

            foreach ($meeting_records as $key => $value) {

                $value->meeting_details = $value->meeting;

                $value->is_owner = NO;

                if($request->id == $value->user_id) {
                        
                    $value->is_owner = YES;

                }

                unset($meeting_records[$key]->meeting);
            }

            $data['records'] = $meeting_records;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_records_view()
     *
     * @uses view recording details
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_records_view(Request $request) {

        try {

            $record_details = \App\MeetingRecord::where('unique_id', $request->meeting_record_unique_id)->first();

            if(!$record_details) {
                throw new Exception(api_error(140), 140);
            }

            $data['record_details'] = $record_details;

            $meeting = \App\Meeting::where('id', $record_details->meeting_id)->first();

            $meeting->is_owner = $meeting->user_id == $request->id ? YES : NO;

            $data['meeting_details'] = $meeting;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_records_save()
     *
     * @uses meeting records end
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_records_save(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_unique_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting_details = \App\Meeting::where('meetings.unique_id', $request->meeting_unique_id)->first();

            if(!$meeting_details) {
                throw new Exception(api_error(139), 139);
            }

            $response = \App\Repositories\BBBRepository::getRecordings($request, $meeting_details)->getData();

            if($response->success == false) {

                throw new Exception($response->error, $response->error_code);
            }

            $check_recording = count((array)$response->data->recordings->recording);

            if($check_recording) {

                $recording_response = $response->data->recordings->recording;

                $meeting_details->is_recordings = YES;

                $meeting_details->bbb_record_id = $recording_response->recordID ?? "";

                $meeting_details->recording_url = $recording_response->playback->format->url ?? "";

                $meeting_details->save();

                // get the meeting records

                $meeting_record = \App\MeetingRecord::where('meeting_id', $meeting_details->meeting_id)->first() ?: new \App\MeetingRecord;

                $meeting_record->meeting_id = $meeting_details->meeting_id;

                $meeting_record->user_id = $meeting_details->user_id;

                $meeting_record->bbb_record_id = $recording_response->recordID ?? "";

                $meeting_record->playback_url = $recording_response->playback->format->url ?? "";

                $meeting_record->overall_record_url = get_overall_record_url($meeting_record->bbb_record_id);

                $meeting_record->video_record_url = get_video_record_url($meeting_record->bbb_record_id);

                $meeting_record->save();
            
            } else {
                // No records available
            }

            $data['recording_response'] = $recording_response ?? [];

            DB::commit();

            return $this->sendResponse($message = api_success(127) , $code = 127, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method recordings_delete()
     *
     * @uses sub instructors remove
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_records_delete(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_record_id' => 'required|exists:meeting_records,id'];

            $custom_errors = ['meeting_record_id' => api_error(140)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $meeting_record = \App\MeetingRecord::where('meeting_records.id', $request->meeting_record_id)->where('user_id', $request->id)->first();

            $meeting = $meeting_record->meeting;

            $response = \App\Repositories\BBBRepository::recordings_delete($request, $meeting)->getData();

            if($response->success == false) {

                throw new Exception($response->error, $response->error_code);
            }

            $meeting_record->delete();

            DB::commit();

            return $this->sendResponse($message = api_success(120) , $code = 120, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_save_jitsi()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_save_jitsi(Request $request) {

        try {

            // Check the user is subscribed any plan

            DB::beginTransaction();

            $meeting = new \App\Meeting;

            $meeting->user_id = $request->id;

            $meeting->title = $request->title ?: "My Meeting ".rand(10000, 99999);

            $meeting->description = $request->description ?: "My Meeting ".rand(10000, 99999);

            $meeting->schedule_time = $meeting->start_time = date('Y-m-d H:i:s');

            $subscription_payment = \App\SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->first();

            $no_of_users = Setting::get('max_no_of_users', 3);

            $no_of_minutes = Setting::get('max_no_of_minutes', 30);

            if($subscription_payment) {

                $no_of_users = $subscription_payment->no_of_users ?? 5;

                $no_of_minutes = $subscription_payment->no_of_minutes ?? 5;

            }

            $meeting->no_of_users = $no_of_users;

            $meeting->no_of_minutes = $no_of_minutes;

            $meeting->status = MEETING_SCHEDULED;

            $meeting->save();

            DB::commit();

            $data['meeting'] = $meeting;

            $data['meeting_id'] = $meeting->meeting_id;

            $data['meeting_unique_id'] = $meeting->meeting_unique_id;

            DB::commit();

            return $this->sendResponse($message = api_success(119) , $code = 119, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_join_jitsi()
     *
     * @uses join the meeting room
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_join_jitsi(Request $request) {

        try {

            // Check the user is subscribed any plan

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_unique_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting = \App\Meeting::where('meetings.unique_id', $request->meeting_unique_id)->first();

            if(!$meeting) {
                throw new Exception(api_error(139), 139);
            }

            $user_id = $request->id ?: 0;

            if(($meeting->status != MEETING_STARTED) && ($meeting->user_id != $user_id)) {

                $error = get_meeting_status_error($meeting->status);

                throw new Exception($error, 141);

            }

            $meeting_user = new \App\MeetingUser;

            $meeting_user->meeting_id = $meeting->meeting_id;

            $meeting_user->user_id = $request->id ?: 0;

            $meeting_user->username = $request->username ?: ($this->loginUser->name ?? "User".rand(10000, 99999));

            $meeting_user->start_time = date('Y-m-d H:i:s');

            $meeting_user->save();

            DB::commit();

            $data['meeting'] = $meeting;

            return $this->sendResponse($message = api_success(125) , $code = 125, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }


    /**
     * @method meetings_end_jitsi()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_end_jitsi(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_unique_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting = \App\Meeting::where('unique_id', $request->meeting_unique_id)->first();

            if(!$meeting) {
                throw new Exception(api_error(139), 139);
            }

            if($request->id == $meeting->user_id) {

                $meeting->status = MEETING_ENDED;

                $meeting->start_time = $meeting->start_time ?: date('Y-m-d H:i:s');

                $meeting->end_time = date('Y-m-d H:i:s');

                $meeting->save();

                DB::commit();
            }

            // $frontend_url = Setting::get('frontend_url').'meetings';

            // return redirect()->away($frontend_url);

            return $this->sendResponse($message = api_success(119) , $code = 119, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            // $frontend_url = Setting::get('frontend_url').'meetings';

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

}
